// Set up Express
var express = require('express');
var app = express();

// Create server and link to Express
var server = require('http').Server(app);

// Associate socket.io with server
var io = require('socket.io')(server);

// Set up public folder hosting
app.use(express.static('public'));

// Game server variables
var players = {};
var playerSockets = {};
var currentGames = {};
var gameCounter = 0;
var startingBoard = [
    [ // Row 0
        {squareColor : 'white', pieceColor: null, pieceType: null},         // Column 0
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},   // Column 1
        {squareColor : 'white', pieceColor: null, pieceType: null},         // Column 2
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},   // Column 3
        {squareColor : 'white', pieceColor: null, pieceType: null},         // Column 4
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},   // Column 5
        {squareColor : 'white', pieceColor: null, pieceType: null},         // Column 6
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'}    // Column 7
    ],
    [ // Row 1
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null}
    ],
    [ // Row 2
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'red', pieceType: 'regular'}
    ],
    [ // Row 3
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null}
    ],
    [ // Row 4
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: null, pieceType: null}
    ],
    [ // Row 5
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null}
    ],
    [ // Row 6
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'}
    ],
    [ // Row 7
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null},
        {squareColor : 'black', pieceColor: 'black', pieceType: 'regular'},
        {squareColor : 'white', pieceColor: null, pieceType: null}
    ]
];

// Set up websocket connection
io.on('connection', function (socket) {
    console.log('new client connected:', socket.id);
    
    // Add new client to main lobby room
    socket.join('main lobby');
    // greet new client
    socket.emit('chatMessage', {sender:'Server', message:'welcome to Checkers!'});

    // create player object
    socket.emit('player info', socket.id);
    socket.on('player info', function (data) {
        if (players.hasOwnProperty(socket.id)) {
            players[socket.id].name = data;
        } else {
            players[socket.id] = {
                name : data,
                id : socket.id,
                invitedBy : [],
                gameId : null
            };
            playerSockets[socket.id] = socket;
        }
        io.to('main lobby').emit('players update', players);
    });
    
    // if a chat message is received from the client, send it to everyone in main lobby
    socket.on('chatMessage', function (data) {
        io.to('main lobby').emit('chatMessage', {sender: data.sender, message: data.message});
    });

    socket.on('invite', function (invitedPlayer) {
        console.log(players[socket.id].name, 'invited', players[invitedPlayer].name, 'to a game');
        players[invitedPlayer].invitedBy.push(socket.id);
        io.to('main lobby').emit('players update', players);
    });

    socket.on('uninvite', function (uninvitedPlayer) {
        console.log(players[socket.id].name, 'uninvited', players[uninvitedPlayer].name);
        for (var i = 0; i < players[uninvitedPlayer].invitedBy.length; i++) {
            if (players[uninvitedPlayer].invitedBy[i] == socket.id) {
                players[uninvitedPlayer].invitedBy.splice(i, 1);
            }
        }
        io.to('main lobby').emit('players update', players);
    });

    socket.on('accept invite', function (acceptedPlayer) {
        // remove all invites for both players
        clearAllInvitations(socket.id);
        clearAllInvitations(acceptedPlayer);

        gameCounter++;
        var gameId = 'game' + gameCounter;

        // update the gameId of both players
        players[socket.id].gameId = gameId;
        players[acceptedPlayer].gameId = gameId;
        io.to('main lobby').emit('players update', players);
        
        // create new game
        currentGames[gameId] = [socket.id, acceptedPlayer];

        // join both players to a seperate room for their game
        socket.join(gameId);
        playerSockets[acceptedPlayer].join(gameId);

        var returnData = {
            gameId : gameId,
            startingColors : {},
            board : startingBoard
        };
        // randomly assign a color to each player
        var coinToss = Math.floor(Math.random() * Math.floor(2));
        if (coinToss == 1) {
            returnData.startingColors[socket.id] = 'black';
            returnData.startingColors[acceptedPlayer] = 'red';
        } else {
            returnData.startingColors[socket.id] = 'red';
            returnData.startingColors[acceptedPlayer] = 'black';
        }

        io.to(gameId).emit('start game', returnData);
    });

    socket.on('end turn', function (data) {
        io.to(data.gameId).emit('next turn', data);
    });

    socket.on('won game', function (data) {
        removeGame(data.gameId);
        io.to(data.gameId).emit('end game', {winner : socket.id});
    });

    socket.on('leave game', function (data) {
        removeGame(data.gameId);
        io.to(data.gameId).emit('end game', {leaver : players[socket.id].name});
    });

    // if client disconnects, remove their player from the server
    socket.on('disconnect', function () {
        if (players[socket.id].gameId != null) {
            io.to(players[socket.id].gameId).emit('end game', {leaver : players[socket.id].name});
            removeGame(players[socket.id].gameId);            
        }
        clearAllInvitations(socket.id);
        delete players[socket.id];
        io.to('main lobby').emit('players update', players);
        delete playerSockets[socket.id];
    });
});

// Start Server
server.listen(4000, function () {
    console.log('server running on port 4000');
})

// Helper Methods
function clearAllInvitations (playerId) {
    for (var playerKey in players) {
        if (players.hasOwnProperty(playerKey)) {
            var player = players[playerKey];
            for (var i = 0; i < player.invitedBy.length; i++) {
                if (player.invitedBy[i] == playerId) {
                    player.invitedBy.splice(i, 1);
                }
            }
        }
    }
}

function removeGame (game) {
    for (var i = 0; i < currentGames[game].length; i++) {
        players[currentGames[game][i]].gameId = null;
    }
    delete currentGames[game];
    io.to('main lobby').emit('players update', players);
}