// Socket connection
var socket = io();

// server will initially give Id and ask for name
socket.on('player info', function (id) {
    chat.userInfo.id = id;
    console.log(chat.userInfo.id);
    console.log('asked for player info');
    socket.emit('player info', chat.userInfo.name);
});

// handle incomming chat messages
socket.on('chatMessage', function (data) {
    chat.globalChat.push(data);
    // scroll chat window to bottom
    var chatbox = document.getElementById('globalChatBox');
    chatbox.scrollTop = chatbox.scrollHeight;
});

socket.on('players update', function (data) {
    chat.onlinePlayers = data;
    //console.log(chat.onlinePlayers);
});

socket.on('start game', function (gameData) {
    console.log(gameData);
    gameBoard.gameIsActive = true;
    gameBoard.myColor = gameData.startingColors[chat.userInfo.id];
    gameBoard.gameId = gameData.gameId;
    gameBoard.board = gameData.board;
});

socket.on('next turn', function (data) {
    console.log(data);
    gameBoard.currentTurn = data.turn;
    gameBoard.board = data.board;
});

socket.on('end game', function (data) {
    if (data.leaver != undefined) {
        if (data.leaver != chat.userInfo.name) {
            alert(data.leaver + ' has left the game');
        }
    } else {
        if (data.winner == chat.userInfo.id) {
            alert('You Have Won!');
        } else {
            alert('You Have Lost!');
        }
    }

    gameBoard.gameIsActive = false;
    gameBoard.myColor = '';
    gameBoard.gameId = '';
    gameBoard.board = [];
});

// Chat Vue Instance
var chat = new Vue({
    el:'#chat',
    data: {
        globalChat: [],
        userInfo: {
            name: 'Joe',
            id: ''
        },
        currentMessage: '',
        onlinePlayers: []
    },
    methods: {
        updateName: function (newName) {
            console.log('Changed name to: ' + newName);
            socket.emit('player info', newName);
        },
        sendMessage: function (name, message) {
            console.log('User: ' + name);
            console.log('Message: ' + message);
            socket.emit('chatMessage', {
                sender: name,
                message: message
            });
            this.currentMessage = '';
        },
        checkIfInviting: function (player) {
            var self = this;
            if (self.onlinePlayers[self.userInfo.id] != undefined) {
                for (var i = 0; i < self.onlinePlayers[player.id].invitedBy.length; i++) {
                    if (self.userInfo.id == self.onlinePlayers[player.id].invitedBy[i]) {
                        return true;
                    }
                }
            }
            return false;
        },
        checkIfInvited: function (player) {
            var self = this;
            if (self.onlinePlayers[self.userInfo.id] != undefined) {
                for (var i = 0; i < self.onlinePlayers[self.userInfo.id].invitedBy.length; i++) {
                    if (player.id == self.onlinePlayers[self.userInfo.id].invitedBy[i]) {
                        return true;
                    }
                }
            }
            return false;
        },
        playerClickHandler: function (player) {
            if (this.checkIfInvited(player)) {      // has this player invited you to a game?
                console.log('challenge accepted!');
                socket.emit('accept invite', player.id);
            } else if (this.checkIfInviting(player)) {  // has this player been invited by you to a game?
                console.log('forget it');
                socket.emit('uninvite', player.id);
            } else {        // this is a new player
                this.inviteToGame(player);
            }
        },
        inviteToGame: function (player) {
            if (player.id != this.userInfo.id) {
                console.log('invited ' + player.name + ' to play');
                socket.emit('invite', player.id);
            }
        }
    }
});

// Gameboard Vue Instance
var gameBoard = new Vue({
    el: '#gameBoard',
    data: {
        gameIsActive: false,
        board: [],
        myColor: '',
        currentTurn: 'black',
        displayedMoves: [],
        colorCounts:  {
            black : 16,
            red : 16
        },
        gameId: '',
        firstMove: true
    },
    methods: {
        processSquare: function (row, col) {
            var self = this;
            if (self.myColor == self.currentTurn) {
                // it is my turn
                if (isMove(row, col)) {
                    // clicked on a highlighted move
                    makeMove(row, col);  
                    checkForKing(row, col);
                    if (self.board[row][col].validMove.moveType == 'jump') {
                        // move jumped an opponent's piece
                        clearValidMoveData();
                        var validMoves = getValidMoves(row, col, true);
                        if (validMoves.length > 0) {
                            for (var i = 0; i < validMoves.length; i++) {
                                var move = validMoves[i];
                                //move.origin = {row: row, col: col};
                                // self.board[move.row][move.col].validMove = move;
                                // self.displayedMoves.push({row: move.row, col: move.col});
                                updateValidMoves(move);
                            }
                            self.firstMove = false;
                        } else {
                            // no more moves completes turn
                            clearValidMoveData();
                            self.firstMove = true;
                            finishTurn();    
                        }
                    } else {
                        // regular move completes turn
                        clearValidMoveData();
                        self.firstMove = true;
                        finishTurn();
                    }
                } else if (!self.firstMove) {
                    // jumped once but chose not to jump again
                    // complete turn
                    clearValidMoveData();
                    self.firstMove = true;
                    finishTurn();
                } else if (isValidPiece(row, col, self.myColor)) {
                    // selected a valid piece
                    clearValidMoveData();
                    var validMoves = getValidMoves(row, col);
                    if (validMoves.length > 0) {
                        for (var i = 0; i < validMoves.length; i++) {
                            var move = validMoves[i];
                            //move.origin = {row: row, col: col};
                            // self.board[move.row][move.col].validMove = move;
                            // self.displayedMoves.push({row: move.row, col: move.col});
                            updateValidMoves(move);
                        }
                    }
    
                }                
            }
        },
        leaveGame: function () {
            socket.emit('leave game', {gameId: this.gameId});
        }
    }
});

function updateValidMoves (move) {
    gameBoard.board[move.row][move.col].validMove = move;
    gameBoard.displayedMoves.push({row: move.row, col: move.col});
    // Don't know why this is necissary, but component won't render without it
    gameBoard.$forceUpdate();
}

function isValidPiece (row, col, color) {
    return gameBoard.board[row][col].pieceColor == color;
}

function isMove (row, col) {
    return gameBoard.board[row][col].validMove != undefined;
}

function makeMove (row, col) {
    var move = gameBoard.board[row][col].validMove;
    if (move.moveType == 'move') {
        var previousPiece;
        if (move.direction == 'up-left') {
            previousPiece = gameBoard.board[move.row + 1][move.col + 1];
        } else if (move.direction == 'up-right') {
            previousPiece = gameBoard.board[move.row + 1][move.col - 1];
        } else if (move.direction == 'down-left') {
            previousPiece = gameBoard.board[move.row - 1][move.col + 1];
        } else if (move.direction == 'down-right') {
            previousPiece = gameBoard.board[move.row - 1][move.col - 1];
        }
        gameBoard.board[row][col].pieceColor = previousPiece.pieceColor;
        gameBoard.board[row][col].pieceType = previousPiece.pieceType;
        previousPiece.pieceColor = null;
        previousPiece.pieceType = null;
    } else if (move.moveType == 'jump') {
        var previousPiece;
        var jumpedPiece;
        if (move.direction == 'up-left') {
            previousPiece = gameBoard.board[move.row + 2][move.col + 2];
            jumpedPiece = gameBoard.board[move.row + 1][move.col + 1];
        } else if (move.direction == 'up-right') {
            previousPiece = gameBoard.board[move.row + 2][move.col - 2];
            jumpedPiece = gameBoard.board[move.row + 1][move.col - 1];
        } else if (move.direction == 'down-left') {
            previousPiece = gameBoard.board[move.row - 2][move.col + 2];
            jumpedPiece = gameBoard.board[move.row - 1][move.col + 1];
        } else if (move.direction == 'down-right') {
            previousPiece = gameBoard.board[move.row - 2][move.col - 2];
            jumpedPiece = gameBoard.board[move.row - 1][move.col - 1];
        }
        gameBoard.board[row][col].pieceColor = previousPiece.pieceColor;
        gameBoard.board[row][col].pieceType = previousPiece.pieceType;
        previousPiece.pieceColor = null;
        previousPiece.pieceType = null;
        jumpedPiece.pieceColor = null;
        jumpedPiece.pieceType = null;
    }
}

function checkForKing (row, col) {
    var piece = gameBoard.board[row][col];
    if (piece.pieceType != 'king') {
        if (row == 0 && piece.pieceColor == 'black') {
            piece.pieceType = 'king';
            return true;
        }
        if (row == 7 && piece.pieceColor == 'red') {
            piece.pieceType = 'king';
            return true;
        }
    }
    return false;
}

function clearValidMoveData () {
    for (var i = 0; i < gameBoard.displayedMoves.length; i++) {
        var currentMove = gameBoard.displayedMoves[i];
        delete gameBoard.board[currentMove.row][currentMove.col].validMove;
    }
    gameBoard.displayedMoves = [];
}

function getValidMoves (row, col, jumpsOnly) {
    jumpsOnly = jumpsOnly ? jumpsOnly : false;
    var validMoves = [];
    var currentPiece = gameBoard.board[row][col];
    if (currentPiece.pieceColor == 'black') {
        if (currentPiece.pieceType == 'regular') {
            var potentialMoves = [
                {row: row - 1, col: col - 1, direction: 'up-left'},
                {row: row - 1, col: col + 1, direction: 'up-right'}
            ];
            validMoves = validateMoves(potentialMoves, currentPiece.pieceColor, jumpsOnly);
        } else if (currentPiece.pieceType == 'king') {
            var potentialMoves = [
                {row: row - 1, col: col - 1, direction: 'up-left'},
                {row: row - 1, col: col + 1, direction: 'up-right'},
                {row: row + 1, col: col - 1, direction: 'down-left'},
                {row: row + 1, col: col + 1, direction: 'down-right'}
            ];
            validMoves = validateMoves(potentialMoves, currentPiece.pieceColor, jumpsOnly);
        }
    }
    if (currentPiece.pieceColor == 'red') {
        if (currentPiece.pieceType == 'regular') {
            var potentialMoves = [
                {row: row + 1, col: col - 1, direction: 'down-left'},
                {row: row + 1, col: col + 1, direction: 'down-right'}
            ];
            validMoves = validateMoves(potentialMoves, currentPiece.pieceColor, jumpsOnly);
        } else if (currentPiece.pieceType == 'king') {
            var potentialMoves = [
                {row: row + 1, col: col - 1, direction: 'down-left'},
                {row: row + 1, col: col + 1, direction: 'down-right'},
                {row: row - 1, col: col - 1, direction: 'up-left'},
                {row: row - 1, col: col + 1, direction: 'up-right'}
            ];
            validMoves = validateMoves(potentialMoves, currentPiece.pieceColor, jumpsOnly);
        }
    }
    return validMoves;
}

function validateMoves (potentialMoves, pieceColor, jumpsOnly) {
    var validMoves = [];
    for (var i = 0; i < potentialMoves.length; i++) {
        var moveToCheck = potentialMoves[i];
        var moveStatus = checkSquare(moveToCheck.row, moveToCheck.col);

        // move is off the board or taken by your own piece
        if (
            moveStatus == 'invalid' ||
            moveStatus == pieceColor
        ) {
            continue;
        }
        // move is available
        if (moveStatus == 'open' && !jumpsOnly) {
            moveToCheck.moveType = 'move';
            validMoves.push(moveToCheck);
            continue;
        }

        // check if jump is available
        if (moveToCheck.direction == 'up-left') {
            var jumpMove = {row: moveToCheck.row - 1, col: moveToCheck.col -1, direction: moveToCheck.direction, moveType: 'jump'};
            var jumpMoveStatus = checkSquare(jumpMove.row, jumpMove.col);
            // make sure square being jumped is opponent and destination square is open
            if (jumpMoveStatus == 'open' && moveStatus != 'open') {
                validMoves.push(jumpMove);
            }
        } else if (moveToCheck.direction == 'up-right') {
            jumpMove = {row: moveToCheck.row - 1, col: moveToCheck.col + 1, direction: moveToCheck.direction, moveType: 'jump'};
            var jumpMoveStatus = checkSquare(jumpMove.row, jumpMove.col);
            if (jumpMoveStatus == 'open' && moveStatus != 'open') {
                validMoves.push(jumpMove);
            }
        } else if (moveToCheck.direction == 'down-left') {
            var jumpMove = {row: moveToCheck.row + 1, col: moveToCheck.col -1, direction: moveToCheck.direction, moveType: 'jump'};
            var jumpMoveStatus = checkSquare(jumpMove.row, jumpMove.col);
            if (jumpMoveStatus == 'open' && moveStatus != 'open') {
                validMoves.push(jumpMove);
            }
        } else if (moveToCheck.direction == 'down-right') {
            jumpMove = {row: moveToCheck.row + 1, col: moveToCheck.col + 1, direction: moveToCheck.direction, moveType: 'jump'};
            var jumpMoveStatus = checkSquare(jumpMove.row, jumpMove.col);
            if (jumpMoveStatus == 'open' && moveStatus != 'open') {
                validMoves.push(jumpMove);
            }
        }
    }
    return validMoves;
}

function checkSquare (row, col) {
    if (
        row < 0 ||
        row > 7 ||
        col < 0 ||
        col > 7
    ) {
        return 'invalid';
    }
    if (gameBoard.board[row][col].pieceColor == null) {
        return 'open';
    }
    return gameBoard.board[row][col].pieceColor;
}

function countColors () {
    var colors = {
        black: 0,
        red: 0
    };
    for (var row = 0; row < gameBoard.board.length; row++) {
        for (var col = 0; col < gameBoard.board[row].length; col++) {
            if (gameBoard.board[row][col].pieceColor == 'black') {
                colors.black++;
            } else if (gameBoard.board[row][col].pieceColor == 'red') {
                colors.red++;
            }
        }
    }
    return colors;
}

function finishTurn () {
    // change turn color
    if (gameBoard.currentTurn == 'black') {
        gameBoard.currentTurn = 'red';
    } else {
        gameBoard.currentTurn = 'black';
    }

    // update the other player's board and make it their turn
    socket.emit('end turn', {
        gameId : gameBoard.gameId,
        turn : gameBoard.currentTurn,
        board : gameBoard.board 
    });

    // if one player has run out of pieces, end the game
    gameBoard.colorCounts = countColors();
    if (gameBoard.myColor == 'black' && gameBoard.colorCounts.red == 0) {
        socket.emit('won game', {gameId : gameBoard.gameId});
    } else if (gameBoard.myColor == 'red' && gameBoard.colorCounts.black == 0) {
        socket.emit('won game', {gameId : gameBoard.gameId});
    }        
}